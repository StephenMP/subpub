﻿namespace SubPub.Enums
{
    public enum IntegrationType
    {
        None = 0,
        Youtube = 1,
        Twitter = 2,
        Discord = 3
    }
}

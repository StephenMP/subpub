﻿using System.Collections.Generic;

namespace SubPub.Models.ViewModels.Integration
{
    public class IntegrationIndexIntegrationModel
    {
        #region Public Properties

        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public string AvatarUrl { get; set; }

        #endregion Public Properties
    }

    public class IntegrationIndexModel
    {
        #region Public Constructors

        public IntegrationIndexModel()
        {
            this.IntegrationModels = new List<IntegrationIndexIntegrationModel>();
        }

        #endregion Public Constructors

        #region Public Properties

        public List<IntegrationIndexIntegrationModel> IntegrationModels { get; }

        #endregion Public Properties
    }
}

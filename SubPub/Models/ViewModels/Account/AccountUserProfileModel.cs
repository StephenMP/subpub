﻿namespace SubPub.Models.ViewModels.Account
{
    public class AccountUserProfileModel
    {
        #region Public Properties

        public string EmailAddress { get; set; }

        #endregion Public Properties
    }
}

﻿namespace SubPub.AppStart.Options
{
    public class DiscordCredentialOptions
    {
        #region Public Properties

        public string BotToken { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        #endregion Public Properties
    }
}

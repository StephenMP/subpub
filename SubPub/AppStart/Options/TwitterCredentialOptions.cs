﻿namespace SubPub.AppStart.Options
{
    public class TwitterCredentialOptions
    {
        #region Public Properties

        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }

        #endregion Public Properties
    }
}

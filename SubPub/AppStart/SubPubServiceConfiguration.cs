﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SubPub.AppStart.Options;
using SubPub.Mapping;
using SubPub.Repository.LiteDb.Account;
using SubPub.Repository.LiteDb.Integration;
using SubPub.Services.Authentication;
using SubPub.Services.Integration;
using SubPub.Services.User;

namespace SubPub.AppStart
{
    public static class SubPubServiceConfiguration
    {
        #region Public Methods

        public static void AddSubPubServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IIntegrationHistoryRepository, IntegrationHistoryRepository>();
            services.AddScoped<IIntegrationRepository, IntegrationRepository>();
            services.AddScoped<IIntegrationService, IntegrationService>();

            services.AddScoped<IOauthService, OauthService>();
            services.AddScoped<IClaimsService, ClaimsService>();
            services.AddScoped<IAuth0Service, Auth0Service>();

            services.AddSingleton(BuildAutoMapper());

            services.Configure<TwitterCredentialOptions>(option =>
            {
                option.ConsumerKey = configuration["TwitterConsumerCredentialOptions:ConsumerKey"];
                option.ConsumerSecret = configuration["TwitterConsumerCredentialOptions:ConsumerSecret"];
            });

            services.Configure<DiscordCredentialOptions>(option =>
            {
                option.BotToken = configuration["DiscordCredentialOptions:BotToken"];
                option.ClientId = configuration["DiscordCredentialOptions:ClientId"];
                option.ClientSecret = configuration["DiscordCredentialOptions:ClientSecret"];
            });
        }

        public static IMapper BuildAutoMapper()
        {
            var mapper = new MapperConfiguration(configure =>
            {
                configure.AddProfile<AutoMapperProfile>();
            }).CreateMapper();

            return mapper;
        }

        #endregion Public Methods
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SubPub.Extensions;
using SubPub.Models.ViewModels.Integration;
using SubPub.Services.Authentication;
using SubPub.Services.Integration;
using SubPub.Services.User;
using System.Threading.Tasks;
using Tweetinvi.Credentials.Models;

namespace SubPub.Controllers
{
    [Authorize]
    public class IntegrationController : Controller
    {
        #region Private Fields

        private readonly IClaimsService claimsService;
        private readonly IIntegrationService integrationService;
        private readonly IUserService subPubUserService;
        private readonly IOauthService thirdPartyAuthenticationService;

        #endregion Private Fields

        #region Public Constructors

        public IntegrationController
        (
            IClaimsService claimsService,
            IUserService subPubUserService,
            IOauthService thirdPartyAuthenticationService,
            IIntegrationService integrationService
        )
        {
            this.claimsService = claimsService;
            this.subPubUserService = subPubUserService;
            this.thirdPartyAuthenticationService = thirdPartyAuthenticationService;
            this.integrationService = integrationService;
        }

        #endregion Public Constructors

        #region Public Methods

        [HttpPost]
        public async Task<string> AddDiscordIntegration()
        {
            var protocol = Request.IsHttps ? "https" : "http";
            var redirectUrl = $"{protocol}://{Request.Host}/{Url.Action(nameof(this.ChallengeDiscordAuthentications)).TrimStart('/')}";
            var authorizationRequestUrl = this.thirdPartyAuthenticationService.ObtainDiscordAuthorizationUrl(redirectUrl);

            return await Task.FromResult(authorizationRequestUrl);
        }

        [HttpPost]
        public async Task<string> AddTwitterIntegration()
        {
            var protocol = Request.IsHttps ? "https" : "http";
            var redirectUrl = $"{protocol}://{Request.Host}/{Url.Action(nameof(this.ChallengeTwitterAuthentication)).TrimStart('/')}";
            var authenticationContext = await this.thirdPartyAuthenticationService.ObtainTwitterAuthorizationContextAsync(redirectUrl);
            var authToken = authenticationContext.Token as AuthenticationToken;

            TempData.Set("TempTwitterAuthKey", authToken.AuthorizationKey);
            TempData.Set("TempTwitterAuthSecret", authToken.AuthorizationSecret);

            return authenticationContext.AuthorizationURL;
        }

        [HttpPost]
        public async Task<string> AddYouTubeIntegration()
        {
            var protocol = Request.IsHttps ? "https" : "http";
            var redirectUrl = $"{protocol}://{Request.Host}/{Url.Action(nameof(this.ChallengeYoutubeAuthentications)).TrimStart('/')}";
            var authorizationUrl = await this.thirdPartyAuthenticationService.ObtainYoutubeAuthorizationUrlAsync(redirectUrl);

            return authorizationUrl;
        }

        public async Task<IActionResult> ChallengeDiscordAuthentications([FromQuery] string guild_id)
        {
            var userId = this.claimsService.GetUserId(User);
            var guildId = ulong.Parse(guild_id);
            await this.integrationService.AddDiscordIntegrationAsync(userId, guildId);

            return RedirectToAction(nameof(this.Index));
        }

        public async Task<IActionResult> ChallengeTwitterAuthentication()
        {
            var userId = this.claimsService.GetUserId(User);
            var verifierCode = Request.Query["oauth_verifier"].ToString();
            var authTokenKey = TempData.Get<string>("TempTwitterAuthKey");
            var authTokenSecret = TempData.Get<string>("TempTwitterAuthSecret");

            await this.integrationService.AddTwitterIntegration(userId, verifierCode, authTokenKey, authTokenSecret);

            return RedirectToAction(nameof(this.Index));
        }

        public async Task<IActionResult> ChallengeYoutubeAuthentications([FromQuery] string code)
        {
            // TODO Handle if code is null (means uer clicked deny)

            // Get user tokens
            var userId = this.claimsService.GetUserId(User);
            var protocol = Request.IsHttps ? "https" : "http";
            var redirectUrl = $"{protocol}://{Request.Host}/{Url.Action(nameof(this.ChallengeYoutubeAuthentications)).TrimStart('/')}";
            var token = await this.thirdPartyAuthenticationService.ObtainYoutubeTokenFromCodeAsync(userId, code, redirectUrl);

            await this.integrationService.AddYoutubeIntegrationAsync(userId, token);

            return RedirectToAction(nameof(this.Index));
        }

        public async Task<IActionResult> Index()
        {
            var userId = this.claimsService.GetUserId(User);
            var user = await this.subPubUserService.GetUserAsync(userId);
            var userIntegrations = await this.integrationService.GetUserIntegrations(userId);
            var model = new IntegrationIndexModel();

            foreach (var integrations in userIntegrations.Values)
            {
                foreach (var integration in integrations)
                {
                    model.IntegrationModels.Add(new IntegrationIndexIntegrationModel
                    {
                        AccountName = integration.AccountName,
                        AccountId = integration.AccountId,
                        AvatarUrl = integration.AvatarUrl,
                        AccountType = integration.AccountType.ToString().ToLower()
                    });
                }
            }

            ViewData["UserFullName"] = user.FullName;

            return await Task.FromResult(View(model));
        }

        [HttpPost]
        public async Task<bool> RemoveDiscordIntegration(ulong guildId)
        {
            var userId = this.claimsService.GetUserId(User);
            await this.integrationService.RemoveDiscordIntegration(guildId.ToString());

            return true;
        }

        [HttpPost]
        public async Task<bool> RemoveTwitterIntegration(long twitterUserId)
        {
            var userId = this.claimsService.GetUserId(User);
            await this.integrationService.RemoveTwitterIntegrationAsync(twitterUserId.ToString());

            return true;
        }

        [HttpPost]
        public async Task<bool> RemoveYouTubeIntegration(string channelId)
        {
            var userId = this.claimsService.GetUserId(User);
            await this.integrationService.RemoveYoutubeIntegrationAsync(channelId);

            return true;
        }

        #endregion Public Methods
    }
}

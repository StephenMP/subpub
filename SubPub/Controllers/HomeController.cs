﻿using Microsoft.AspNetCore.Mvc;
using SubPub.Models.ViewModels;
using System.Diagnostics;

namespace SubPub.Controllers
{
    public class HomeController : Controller
    {
        #region Public Methods

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Integration");
            }

            return View();
        }

        #endregion Public Methods
    }
}

﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SubPub.Models.ViewModels.Account;
using SubPub.Services.Authentication;
using SubPub.Services.User;
using System.Threading.Tasks;

namespace SubPub.Controllers
{
    public class AccountController : Controller
    {
        #region Private Fields

        private readonly IAuth0Service auth0Service;
        private readonly IClaimsService claimsService;
        private readonly IUserService userService;

        #endregion Private Fields

        #region Public Constructors

        public AccountController(IClaimsService claimsService, IUserService userService, IAuth0Service auth0Service)
        {
            this.claimsService = claimsService;
            this.userService = userService;
            this.auth0Service = auth0Service;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task Login(string returnUrl = "/")
        {
            await HttpContext.ChallengeAsync("Auth0", new AuthenticationProperties { RedirectUri = Url.Action("SignIn", new { returnUrl }) });
        }

        [Authorize]
        public async Task Logout()
        {
            await HttpContext.SignOutAsync("Auth0", new AuthenticationProperties
            {
                RedirectUri = Url.Action("Index", "Home")
            });

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task<IActionResult> SignIn(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                var userId = this.claimsService.GetUserId(User);
                var isFirstSignIn = await this.userService.IsFirstSignInAsync(userId);

                if (isFirstSignIn)
                {
                    await this.userService.BuildUserProfile(User);
                    return RedirectToAction(nameof(this.UserProfile));
                }
                else
                {
                    var subPubUser = await this.userService.GetUserAsync(userId);
                    var auth0User = await this.auth0Service.GetUserAsync(userId);

                    subPubUser.DateOfBirth = auth0User.UserMetadata.date_of_birth;
                    subPubUser.FullName = auth0User.UserMetadata.full_name;

                    await this.userService.UpdateUserAsync(subPubUser);

                    return Redirect(returnUrl);
                }
            }
            else
            {
                return await Task.FromResult(RedirectToAction(nameof(HomeController.Index), nameof(HomeController).Replace("Controller", "")));
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<bool> UpdateUserProfile(string name, string emailAddress)
        {
            var userId = this.claimsService.GetUserId(User);
            await this.userService.UpdateUserProfileAsync(userId, name, emailAddress);

            return true;
        }

        [Authorize]
        public async Task<IActionResult> UserProfile()
        {
            var userId = this.claimsService.GetUserId(User);
            var email = this.claimsService.GetUserEmail(User);
            var user = await this.userService.GetUserAsync(userId);

            var model = new AccountUserProfileModel
            {
                EmailAddress = email,
            };

            ViewData["UserFullName"] = user.FullName;

            return View(model);
        }

        #endregion Public Methods
    }
}

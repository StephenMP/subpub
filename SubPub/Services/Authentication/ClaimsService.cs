﻿using System;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace SubPub.Services.Authentication
{
    public interface IClaimsService
    {
        #region Public Methods

        string GetUserEmail(ClaimsPrincipal userPrincipal);

        string GetUserId(ClaimsPrincipal userPrincipal);

        string GetUserName(ClaimsPrincipal userPrincipal);

        string GetUserPicture(ClaimsPrincipal userPrincipal);

        bool IsUserEmailVerified(ClaimsPrincipal userPrincipal);

        #endregion Public Methods
    }

    public class ClaimsService : IClaimsService
    {
        #region Public Methods

        public string GetUserEmail(ClaimsPrincipal userPrincipal)
        {
            var claim = userPrincipal?.Claims?.FirstOrDefault(c => MatchClaim(c.Type, ClaimTypes.Email));
            return claim?.Value;
        }

        public string GetUserId(ClaimsPrincipal userPrincipal)
        {
            var claim = userPrincipal?.Claims?.FirstOrDefault(c => MatchClaim(c.Type, ClaimTypes.NameIdentifier));
            return claim?.Value;
        }

        public string GetUserName(ClaimsPrincipal userPrincipal)
        {
            var claim = userPrincipal?.Claims?.FirstOrDefault(c => MatchClaim(c.Type, ClaimTypes.Name));
            return claim?.Value;
        }

        public string GetUserPicture(ClaimsPrincipal userPrincipal)
        {
            var claim = userPrincipal?.Claims?.FirstOrDefault(c => MatchClaim(c.Type, "picture"));
            return claim?.Value;
        }

        public bool IsUserEmailVerified(ClaimsPrincipal userPrincipal)
        {
            var claim = userPrincipal?.Claims?.FirstOrDefault(c => MatchClaim(c.Type, "email_verified"));
            return claim.Value == null ? false : Convert.ToBoolean(claim.Value);
        }

        #endregion Public Methods

        #region Private Methods

        private bool MatchClaim(string claimType, string claimMatchType)
        {
            var parsedClaimMatchType = Path.GetFileName(claimMatchType);
            return claimType.Equals(claimMatchType, StringComparison.CurrentCultureIgnoreCase) || claimType.Equals(parsedClaimMatchType, StringComparison.CurrentCultureIgnoreCase);
        }

        #endregion Private Methods
    }
}

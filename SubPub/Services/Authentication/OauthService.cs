﻿using Discord.Rest;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTubeReporting.v1;
using Microsoft.Extensions.Options;
using SubPub.AppStart.Options;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Credentials.Models;
using Tweetinvi.Models;

namespace SubPub.Services.Authentication
{
    public interface IOauthService
    {
        #region Public Methods

        Task<DiscordRestClient> GetDiscordRestClient();

        AuthorizationCodeFlow GetYoutubeAuthorizationCodeFlow(params string[] scopes);

        string ObtainDiscordAuthorizationUrl(string redirectUrl);

        ITwitterCredentials ObtainTwitterAuthenticationUserCredentials(string verifierCode, string authorizationKey, string authorizationSecret);

        Task<IAuthenticationContext> ObtainTwitterAuthorizationContextAsync(string redirectUrl);

        Task<string> ObtainYoutubeAuthorizationUrlAsync(string redirectUrl);

        Task<TokenResponse> ObtainYoutubeTokenFromCodeAsync(string userId, string code, string redirectUrl);

        #endregion Public Methods
    }

    public class OauthService : IOauthService
    {
        #region Private Fields

        private readonly DiscordCredentialOptions discordCredentials;
        private readonly TwitterCredentialOptions twitterCredentials;

        #endregion Private Fields

        #region Public Constructors

        public OauthService(IOptions<TwitterCredentialOptions> twitterOptions, IOptions<DiscordCredentialOptions> discordOptions)
        {
            this.twitterCredentials = twitterOptions.Value;
            this.discordCredentials = discordOptions.Value;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task<DiscordRestClient> GetDiscordRestClient()
        {
            var restClient = new DiscordRestClient();
            await restClient.LoginAsync(Discord.TokenType.Bot, this.discordCredentials.BotToken);

            return restClient;
        }

        public AuthorizationCodeFlow GetYoutubeAuthorizationCodeFlow(params string[] scopes)
        {
            using (var stream = new MemoryStream(Properties.Resources.client_id))
            {
                var clientSecrets = GoogleClientSecrets.Load(stream).Secrets;
                var initializer = new GoogleAuthorizationCodeFlow.Initializer { ClientSecrets = clientSecrets, Scopes = scopes };
                var googleAuthorizationCodeFlow = new GoogleAuthorizationCodeFlow(initializer);

                return googleAuthorizationCodeFlow;
            }
        }

        public string ObtainDiscordAuthorizationUrl(string redirectUrl)
        {
            var uriEncodedRedirect = Uri.EscapeDataString(redirectUrl);
            return $"https://discordapp.com/oauth2/authorize?client_id=412273597084860419&scope=bot&permissions=37215424&response_type=code&redirect_uri={uriEncodedRedirect}";
        }

        public ITwitterCredentials ObtainTwitterAuthenticationUserCredentials(string verifierCode, string authorizationKey, string authorizationSecret)
        {
            var token = new AuthenticationToken
            {
                AuthorizationKey = authorizationKey,
                AuthorizationSecret = authorizationSecret,
                ConsumerCredentials = new ConsumerCredentials(this.twitterCredentials.ConsumerKey, this.twitterCredentials.ConsumerSecret)
            };

            return AuthFlow.CreateCredentialsFromVerifierCode(verifierCode, token);
        }

        public async Task<IAuthenticationContext> ObtainTwitterAuthorizationContextAsync(string redirectUrl)
        {
            var appCred = new ConsumerCredentials(this.twitterCredentials.ConsumerKey, this.twitterCredentials.ConsumerSecret);
            var authenticationContext = AuthFlow.InitAuthentication(appCred, redirectUrl);

            return await Task.FromResult(authenticationContext);
        }

        public async Task<string> ObtainYoutubeAuthorizationUrlAsync(string redirectUrl)
        {
            var scopes = new[] { YouTubeService.Scope.YoutubeForceSsl, YouTubeReportingService.Scope.YtAnalyticsReadonly };
            var googleAuthorizationCodeFlow = this.GetYoutubeAuthorizationCodeFlow(scopes);
            var codeRequestUrl = googleAuthorizationCodeFlow.CreateAuthorizationCodeRequest(redirectUrl);
            codeRequestUrl.ResponseType = "code";

            var authorizationUrl = codeRequestUrl.Build();

            return await Task.FromResult(authorizationUrl.ToString());
        }

        public async Task<TokenResponse> ObtainYoutubeTokenFromCodeAsync(string userId, string code, string redirectUrl)
        {
            var scopes = new[] { YouTubeService.Scope.YoutubeForceSsl, YouTubeReportingService.Scope.YtAnalyticsReadonly };
            var googleAuthorizationCodeFlow = this.GetYoutubeAuthorizationCodeFlow(scopes);
            var response = await googleAuthorizationCodeFlow.ExchangeCodeForTokenAsync(userId, code, redirectUrl, CancellationToken.None);

            return response;
        }

        #endregion Public Methods
    }
}

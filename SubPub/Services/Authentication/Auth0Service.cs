﻿using Auth0.ManagementApi;
using Auth0.ManagementApi.Models;
using Newtonsoft.Json;
using RestWell.Client;
using RestWell.Client.Enums;
using RestWell.Client.Request;
using SubPub.Repository.LiteDb.Account.Entities;
using System;
using System.Threading.Tasks;

namespace SubPub.Services.Authentication
{
    public interface IAuth0Service
    {
        #region Public Methods

        Task<ManagementApiClient> GetManagementApiClient();

        Task<Auth0.Core.User> GetUserAsync(string userId);

        Task UpdateUserMetadataAsync(string userId, LiteDbUserEntity userEntity);

        #endregion Public Methods
    }

    public class Auth0AccessTokenRequest
    {
        #region Public Properties

        [JsonProperty("audience")]
        public string Audience => "https://subpub.auth0.com/api/v2/";

        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("client_secret")]
        public string ClientSecret { get; set; }

        [JsonProperty("grant_type")]
        public string GrantType => "client_credentials";

        #endregion Public Properties
    }

    public class Auth0AccessTokenResponse
    {
        #region Public Properties

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        #endregion Public Properties
    }

    public class Auth0Service : IAuth0Service
    {
        #region Public Methods

        public async Task<ManagementApiClient> GetManagementApiClient()
        {
            var requestDto = new Auth0AccessTokenRequest { ClientId = "yZxO3Jw0TN5f5oIMVziXDJb4eKO335D3", ClientSecret = "jEKWlnYlHSWWi_fg6Hhad65WFlS5Gmj-0cLTitcKP7yvmP1NIH90EfP6m3mmReRO" };
            var request = ProxyRequestBuilder<Auth0AccessTokenRequest, Auth0AccessTokenResponse>
                            .CreateBuilder("https://subpub.auth0.com/oauth/token", HttpRequestMethod.Post)
                            .Accept("application/json")
                            .SetRequestDto(requestDto)
                            .Build();

            using (var proxy = new Proxy())
            {
                var response = await proxy.InvokeAsync(request);

                if (response.IsSuccessfulStatusCode)
                {
                    return new ManagementApiClient(response.ResponseDto.AccessToken, "subpub.auth0.com");
                }
                else
                {
                    throw new Exception(response.ResponseMessage);
                }
            }
        }

        public async Task<Auth0.Core.User> GetUserAsync(string userId)
        {
            var managementClient = await this.GetManagementApiClient();
            var user = await managementClient.Users.GetAsync(userId);

            return user;
        }

        public async Task UpdateUserMetadataAsync(string userId, LiteDbUserEntity userEntity)
        {
            var managementClient = await this.GetManagementApiClient();
            var updateUserRequest = new UserUpdateRequest
            {
                UserMetadata = new { full_name = userEntity.FullName, date_of_birth = userEntity.DateOfBirth }
            };

            await managementClient.Users.UpdateAsync(userId, updateUserRequest);
        }

        #endregion Public Methods
    }
}

﻿using Microsoft.Extensions.Options;
using SubPub.AppStart.Options;
using SubPub.Repository.LiteDb.Account;
using SubPub.Repository.LiteDb.Account.Entities;
using SubPub.Services.Authentication;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SubPub.Services.User
{
    public interface IUserService
    {
        #region Public Methods

        Task BuildUserProfile(ClaimsPrincipal userPrincipal);

        Task<LiteDbUserEntity> GetUserAsync(string userId);

        Task<bool> IsFirstSignInAsync(string userId);

        Task UpdateUserAsync(LiteDbUserEntity subPubUser);

        Task UpdateUserProfileAsync(string userId, string name, string emailAddress);

        #endregion Public Methods
    }

    public class UserService : IUserService
    {
        #region Private Fields

        private readonly IAuth0Service auth0Service;
        private readonly IClaimsService claimsService;
        private readonly DiscordCredentialOptions discordCredentials;
        private readonly IOauthService thirdPartyAuthenticationService;
        private readonly IUserRepository userRepository;

        #endregion Private Fields

        #region Public Constructors

        public UserService(
            IClaimsService claimsService,
            IOauthService thirdPartyAuthenticationService,
            IUserRepository userRepository,
            IAuth0Service auth0Service,
            IOptions<DiscordCredentialOptions> discordCredentialOptions
        )
        {
            this.claimsService = claimsService;
            this.thirdPartyAuthenticationService = thirdPartyAuthenticationService;
            this.userRepository = userRepository;
            this.auth0Service = auth0Service;
            this.discordCredentials = discordCredentialOptions.Value;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task BuildUserProfile(ClaimsPrincipal userPrincipal)
        {
            var userId = this.claimsService.GetUserId(userPrincipal);
            var user = new LiteDbUserEntity { Id = userId };
            var fullName = this.claimsService.GetUserName(userPrincipal);
            user.FullName = fullName;

            await this.auth0Service.UpdateUserMetadataAsync(userId, user);
            await this.userRepository.CreateAsync(user);
        }

        public async Task<LiteDbUserEntity> GetUserAsync(string userId)
        {
            var user = await this.userRepository.ReadAsync(userId);
            return user;
        }

        public string GetUserId(ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Value;
        }

        public async Task<bool> IsFirstSignInAsync(string userId)
        {
            var exists = await this.userRepository.Exists(userId);
            return !exists;
        }

        public async Task UpdateUserAsync(LiteDbUserEntity subPubUser)
        {
            await this.userRepository.UpdateAsync(subPubUser);
        }

        public async Task UpdateUserProfileAsync(string userId, string name, string emailAddress)
        {
            var user = await this.userRepository.ReadAsync(userId);
            user.FullName = name;
            await this.userRepository.UpdateAsync(user);

            await this.auth0Service.UpdateUserMetadataAsync(userId, user);
        }

        #endregion Public Methods

        #region Private Methods

        private async Task EditUser(string userId, Action<LiteDbUserEntity> action)
        {
            var user = await this.userRepository.ReadAsync(userId);
            action(user);
            await this.userRepository.UpdateAsync(user);
        }

        #endregion Private Methods
    }
}

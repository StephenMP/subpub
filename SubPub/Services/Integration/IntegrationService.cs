﻿using AutoMapper;
using Discord.Rest;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestWell.Client;
using RestWell.Client.Enums;
using RestWell.Client.Request;
using SubPub.AppStart.Options;
using SubPub.Enums;
using SubPub.Repository.LiteDb.Integration;
using SubPub.Repository.LiteDb.Integration.Entities;
using SubPub.Services.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubPub.Services.Integration
{
    public interface IIntegrationService
    {
        #region Public Methods

        Task AddDiscordIntegrationAsync(string userId, ulong guildId);

        Task AddTwitterIntegration(string userId, string verifierCode, string authorizationKey, string authorizationSecret);

        Task AddYoutubeIntegrationAsync(string userId, TokenResponse token);

        Task<Dictionary<IntegrationType, List<LiteDbIntegrationEntity>>> GetUserIntegrations(string userId);

        Task RemoveDiscordIntegration(string guildId);

        Task RemoveTwitterIntegrationAsync(string twitterUserId);

        Task RemoveYoutubeIntegrationAsync(string channelId);

        #endregion Public Methods
    }

    public class IntegrationService : IIntegrationService
    {
        #region Private Fields

        private readonly DiscordCredentialOptions discordCredentials;
        private readonly IIntegrationHistoryRepository integrationHistoryRepository;
        private readonly IIntegrationRepository integrationRepository;
        private readonly IMapper mapper;
        private readonly IOauthService oauthService;

        #endregion Private Fields

        #region Public Constructors

        public IntegrationService(IIntegrationRepository integrationRepository, IIntegrationHistoryRepository integrationHistoryRepository, IOauthService oauthService, IMapper mapper, IOptions<DiscordCredentialOptions> options)
        {
            this.integrationRepository = integrationRepository;
            this.integrationHistoryRepository = integrationHistoryRepository;
            this.oauthService = oauthService;
            this.mapper = mapper;
            this.discordCredentials = options.Value;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task AddDiscordIntegrationAsync(string userId, ulong guildId)
        {
            var client = new DiscordRestClient();
            await client.LoginAsync(Discord.TokenType.Bot, this.discordCredentials.BotToken);
            var guild = await client.GetGuildAsync(guildId);

            var discordIntegration = new LiteDbIntegrationEntity(userId, IntegrationType.Discord)
            {
                AccountId = guildId.ToString(),
                AccountName = guild.Name,
                AvatarUrl = guild.IconUrl
            };

            await this.AddUserIntegration(discordIntegration);
        }

        public async Task AddTwitterIntegration(string userId, string verifierCode, string authorizationKey, string authorizationSecret)
        {
            var twitterCredentials = this.oauthService.ObtainTwitterAuthenticationUserCredentials(verifierCode, authorizationKey, authorizationSecret);
            var twitterUser = Tweetinvi.User.GetAuthenticatedUser(twitterCredentials);
            var twitterToken = new LiteDbTwitterToken(twitterUser.Credentials.AccessToken, twitterUser.Credentials.AccessTokenSecret);
            var twitterTokenJson = JsonConvert.SerializeObject(twitterToken);

            var twitterIntegration = new LiteDbIntegrationEntity(userId, IntegrationType.Twitter)
            {
                AccountId = twitterUser.Id.ToString(),
                AccountName = twitterUser.ScreenName,
                AvatarUrl = twitterUser.ProfileImageUrl400x400,
                AccessToken = twitterToken.AccessToken,
                TokenJson = twitterTokenJson
            };

            await this.AddUserIntegration(twitterIntegration);
        }

        public async Task AddYoutubeIntegrationAsync(string userId, TokenResponse token)
        {
            var googleAuthorizationCodeFlow = this.oauthService.GetYoutubeAuthorizationCodeFlow();
            var userCredentials = new UserCredential(googleAuthorizationCodeFlow, userId, token);
            var initializer = new BaseClientService.Initializer { HttpClientInitializer = userCredentials, ApplicationName = "YouTubeTest" };
            var youtubeService = new YouTubeService(initializer);
            var channelListRequest = youtubeService.Channels.List("snippet");
            channelListRequest.Mine = true;

            var channelListResponse = await channelListRequest.ExecuteAsync();
            var channel = channelListResponse.Items.FirstOrDefault();

            var youtubeIntegration = new LiteDbIntegrationEntity(userId, IntegrationType.Youtube)
            {
                AccessToken = token.AccessToken,
                AccountId = channel.Id,
                AccountName = channel.Snippet.Title,
                AvatarUrl = channel.Snippet.Thumbnails.Default__.Url,
                TokenJson = JsonConvert.SerializeObject(token)
            };

            await this.AddUserIntegration(youtubeIntegration);
        }

        public async Task<Dictionary<IntegrationType, List<LiteDbIntegrationEntity>>> GetUserIntegrations(string userId)
        {
            var integrations = await this.integrationRepository.ReadAllUserIntegrationsAsync(userId);
            var integrationDictionary = new Dictionary<IntegrationType, List<LiteDbIntegrationEntity>>();

            foreach (var integration in integrations)
            {
                if (integrationDictionary.ContainsKey(integration.AccountType))
                {
                    integrationDictionary[integration.AccountType].Add(integration);
                }
                else
                {
                    integrationDictionary.Add(integration.AccountType, new List<LiteDbIntegrationEntity> { integration });
                }
            }

            return integrationDictionary;
        }

        public async Task RemoveDiscordIntegration(string guildId)
        {
            await this.RemoveUserIntegration(guildId);

            var discordRestClient = await this.oauthService.GetDiscordRestClient();
            var guild = await discordRestClient.GetGuildAsync(ulong.Parse(guildId));
            await guild.LeaveAsync();
        }

        public async Task RemoveTwitterIntegrationAsync(string twitterUserId)
        {
            await this.RemoveUserIntegration(twitterUserId);
        }

        public async Task RemoveYoutubeIntegrationAsync(string channelId)
        {
            var integration = await this.integrationRepository.ReadByAccountId(channelId);
            var accessToken = integration.AccessToken;

            await this.RemoveUserIntegration(channelId);

            var request = ProxyRequestBuilder.CreateBuilder("https://accounts.google.com/o/oauth2/revoke", HttpRequestMethod.Get).AddQueryParameter("token", accessToken).Build();
            using (var proxy = new Proxy())
            {
                var response = await proxy.InvokeAsync(request);
                if (!response.IsSuccessfulStatusCode)
                {
                    // Should probably find a way to tell the user they need to manually remove access in their Google settings
                }
            }
        }

        #endregion Public Methods

        #region Private Methods

        private async Task AddUserIntegration(LiteDbIntegrationEntity integration)
        {
            await this.integrationRepository.CreateAsync(integration);
            var integrationHistory = this.mapper.Map<LiteDbIntegrationHistoryEntity>(integration);
            await this.integrationHistoryRepository.CreateAsync(integrationHistory);
        }

        private async Task RemoveUserIntegration(string accountId)
        {
            await this.integrationRepository.DeleteByAccountIdAsync(accountId);
            var integrationHistoryEntity = await this.integrationHistoryRepository.ReadByAccountIdAsync(accountId);
            integrationHistoryEntity.DateRemoved = DateTime.Now;

            await this.integrationHistoryRepository.UpdateAsync(integrationHistoryEntity);
        }

        #endregion Private Methods
    }
}

﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;

namespace SubPub.Extensions
{
    public static class TempDataExtensions
    {
        #region Public Methods

        public static T Get<T>(this ITempDataDictionary tempData, string key)
        {
            var value = tempData[key] as string;
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public static void Set<T>(this ITempDataDictionary tempData, string key, T value)
        {
            var jsonValue = JsonConvert.SerializeObject(value);
            tempData[key] = jsonValue;
        }

        #endregion Public Methods
    }
}

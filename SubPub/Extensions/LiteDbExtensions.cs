﻿using LiteDB;

namespace SubPub.Extensions
{
    public static class LiteDbExtensions
    {
        #region Public Methods

        public static void EnsureIndexes<TEntity>(this LiteCollection<TEntity> collection, params string[] indexes)
        {
            foreach (var index in indexes)
            {
                collection.EnsureIndex(index);
            }
        }

        #endregion Public Methods
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SubPub.Repository
{
    public interface IRepository<TId, TEntity>
    {
        #region Public Methods

        Task<TEntity> CreateAsync(TEntity entity);

        Task DeleteAsync(TEntity entity);

        Task DeleteAsync(TId id);

        Task<bool> Exists(TId id);

        Task<IEnumerable<TEntity>> ReadAllAsync();

        Task<TEntity> ReadAsync(TId id);

        Task<TEntity> UpdateAsync(TEntity entity);

        #endregion Public Methods
    }
}

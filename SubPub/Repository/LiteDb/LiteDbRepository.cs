﻿using LiteDB;
using SubPub.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SubPub.Repository.LiteDb
{
    public abstract class LiteDbRepository<TEntity> : ILiteDbRepository<TEntity> where TEntity : LiteDbEntity
    {
        #region Private Fields

        private readonly string collectionName;
        private readonly string database;

        #endregion Private Fields

        #region Public Constructors

        public LiteDbRepository(string database)
        {
            this.database = database;
            this.collectionName = typeof(TEntity).Name.Replace("Entity", "");
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task<TEntity> CreateAsync(TEntity entity, params string[] indexes)
        {
            var now = DateTime.Now.ToUniversalTime();
            entity.DateCreated = now;
            entity.DateUpdated = now;

            this.LiteDbTransaction(collection =>
            {
                collection.Insert(entity);
                collection.EnsureIndexes(indexes);
            });

            return await Task.FromResult(entity);
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            var now = DateTime.Now.ToUniversalTime();
            entity.DateCreated = now;
            entity.DateUpdated = now;

            this.LiteDbTransaction(collection =>
            {
                collection.Insert(entity);
            });

            return await Task.FromResult(entity);
        }

        public async Task DeleteAsync(TEntity entity)
        {
            await this.DeleteAsync(entity.Id);
        }

        public async Task DeleteAsync(string id)
        {
            await Task.CompletedTask;

            this.LiteDbTransaction(collection =>
            {
                collection.Delete(e => e.Id == id);
            });
        }

        public async Task<bool> Exists(string id)
        {
            var exists = false;

            this.LiteDbTransaction(collection =>
            {
                exists = collection.Exists(e => e.Id == id);
            });

            return await Task.FromResult(exists);
        }

        public async Task<IEnumerable<TEntity>> ReadAllAsync()
        {
            IEnumerable<TEntity> results = null;

            this.LiteDbTransaction(collection =>
            {
                results = collection.FindAll();
            });

            foreach (var result in results)
            {
                result.DateCreated = result.DateCreated.ToLocalTime();
                result.DateUpdated = result.DateUpdated.ToLocalTime();
            }

            return await Task.FromResult(results);
        }

        public async Task<TEntity> ReadAsync(string id)
        {
            TEntity result = null;
            this.LiteDbTransaction(collection =>
            {
                result = collection.FindOne(e => e.Id == id);
            });

            result.DateCreated = result.DateCreated.ToLocalTime();
            result.DateUpdated = result.DateUpdated.ToLocalTime();

            return await Task.FromResult(result);
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            entity.DateUpdated = DateTime.Now.ToUniversalTime();

            this.LiteDbTransaction(collection =>
            {
                collection.Update(entity);
            });

            return await Task.FromResult(entity);
        }

        #endregion Public Methods

        #region Protected Methods

        protected LiteCollection<TEntity> Collection(LiteDatabase database) => database.GetCollection<TEntity>(this.collectionName);

        protected LiteDatabase Database() => new LiteDatabase(this.database);

        protected void LiteDbTransaction(Action<LiteCollection<TEntity>> databaseAction)
        {
            using (var database = Database())
            {
                var collection = Collection(database);
                databaseAction(collection);
            }
        }

        #endregion Protected Methods
    }
}

﻿using System;

namespace SubPub.Repository.LiteDb
{
    public class LiteDbEntity
    {
        #region Public Properties

        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string Id { get; set; }

        #endregion Public Properties
    }
}

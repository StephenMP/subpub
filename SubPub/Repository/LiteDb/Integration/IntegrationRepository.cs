﻿using SubPub.Repository.LiteDb.Integration.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SubPub.Repository.LiteDb.Integration
{
    public interface IIntegrationRepository : ILiteDbRepository<LiteDbIntegrationEntity>
    {
        #region Public Methods

        Task DeleteByAccountIdAsync(string accountId);

        Task<List<LiteDbIntegrationEntity>> ReadAllUserIntegrationsAsync(string userId);

        Task<LiteDbIntegrationEntity> ReadByAccountId(string accountId);

        #endregion Public Methods
    }

    public class IntegrationRepository : LiteDbRepository<LiteDbIntegrationEntity>, IIntegrationRepository
    {
        #region Public Constructors

        public IntegrationRepository() : base("SubPub.Integration.db")
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task DeleteByAccountIdAsync(string accountId)
        {
            await Task.CompletedTask;

            this.LiteDbTransaction(collection =>
            {
                collection.Delete(e => e.AccountId == accountId);
            });
        }

        public async Task<List<LiteDbIntegrationEntity>> ReadAllUserIntegrationsAsync(string userId)
        {
            var results = new List<LiteDbIntegrationEntity>();

            this.LiteDbTransaction(collection =>
            {
                var integrations = collection.Find(e => e.UserId == userId);
                results = integrations.ToList();
            });

            return await Task.FromResult(results);
        }

        public async Task<LiteDbIntegrationEntity> ReadByAccountId(string accountId)
        {
            LiteDbIntegrationEntity result = null;

            this.LiteDbTransaction(collection =>
            {
                result = collection.FindOne(e => e.AccountId == accountId);
            });

            return await Task.FromResult(result);
        }

        #endregion Public Methods
    }
}

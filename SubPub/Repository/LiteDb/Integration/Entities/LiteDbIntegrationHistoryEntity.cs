﻿using SubPub.Enums;
using System;

namespace SubPub.Repository.LiteDb.Integration.Entities
{
    public class LiteDbIntegrationHistoryEntity : LiteDbEntity
    {
        #region Public Properties

        public string AccessToken { get; set; }
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public IntegrationType AccountType { get; set; }
        public DateTime? DateRemoved { get; set; }
        public string IntegrationId { get; set; }
        public string UserId { get; set; }

        #endregion Public Properties
    }
}

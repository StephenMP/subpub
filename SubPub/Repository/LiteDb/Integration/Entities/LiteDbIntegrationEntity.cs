﻿using SubPub.Enums;
using System;

namespace SubPub.Repository.LiteDb.Integration.Entities
{
    public class LiteDbIntegrationEntity : LiteDbEntity
    {
        #region Public Constructors

        public LiteDbIntegrationEntity()
        {
            var now = DateTime.Now;
            this.Id = Guid.NewGuid().ToString("N");
            this.DateCreated = now;
            this.DateUpdated = now;
        }

        public LiteDbIntegrationEntity(string userId, IntegrationType integrationType) : this()
        {
            this.UserId = userId;
            this.AccountType = integrationType;
        }

        #endregion Public Constructors

        #region Public Properties

        public string AccessToken { get; set; }
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public IntegrationType AccountType { get; set; }
        public string AvatarUrl { get; set; }
        public string TokenJson { get; set; }
        public string UserId { get; set; }

        #endregion Public Properties
    }

    public class LiteDbTwitterToken
    {
        #region Public Constructors

        public LiteDbTwitterToken(string accessToken, string accessTokenSecret)
        {
            AccessToken = accessToken;
            AccessTokenSecret = accessTokenSecret;
        }

        #endregion Public Constructors

        #region Public Properties

        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }

        #endregion Public Properties
    }
}

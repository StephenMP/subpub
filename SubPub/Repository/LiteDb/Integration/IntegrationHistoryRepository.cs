﻿using SubPub.Repository.LiteDb.Integration.Entities;
using System.Threading.Tasks;

namespace SubPub.Repository.LiteDb.Integration
{
    public interface IIntegrationHistoryRepository : ILiteDbRepository<LiteDbIntegrationHistoryEntity>
    {
        #region Public Methods

        Task<LiteDbIntegrationHistoryEntity> ReadByAccountIdAsync(string accountId);

        #endregion Public Methods
    }

    public class IntegrationHistoryRepository : LiteDbRepository<LiteDbIntegrationHistoryEntity>, IIntegrationHistoryRepository
    {
        #region Public Constructors

        public IntegrationHistoryRepository() : base("SubPub.Integration.db")
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task<LiteDbIntegrationHistoryEntity> ReadByAccountIdAsync(string accountId)
        {
            LiteDbIntegrationHistoryEntity result = null;

            this.LiteDbTransaction(collection =>
            {
                result = collection.FindOne(e => e.AccountId == accountId);
            });

            return await Task.FromResult(result);
        }

        #endregion Public Methods
    }
}

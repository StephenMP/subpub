﻿using System.Threading.Tasks;

namespace SubPub.Repository.LiteDb
{
    public interface ILiteDbRepository<TEntity> : IRepository<string, TEntity>
    {
        #region Public Methods

        Task<TEntity> CreateAsync(TEntity entity, params string[] indexes);

        #endregion Public Methods
    }
}

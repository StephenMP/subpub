﻿using System;

namespace SubPub.Repository.LiteDb.Account.Entities
{
    public class LiteDbUserEntity : LiteDbEntity
    {
        #region Public Properties

        public DateTime? DateOfBirth { get; set; }
        public string FullName { get; set; }

        #endregion Public Properties
    }
}

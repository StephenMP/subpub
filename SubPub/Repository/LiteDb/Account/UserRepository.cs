﻿using SubPub.Repository.LiteDb.Account.Entities;

namespace SubPub.Repository.LiteDb.Account
{
    public interface IUserRepository : ILiteDbRepository<LiteDbUserEntity> { }

    public class UserRepository : LiteDbRepository<LiteDbUserEntity>, IUserRepository
    {
        #region Public Constructors

        public UserRepository() : base("SubPub.Account.db")
        {
        }

        #endregion Public Constructors
    }
}

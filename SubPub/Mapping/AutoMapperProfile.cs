﻿using AutoMapper;
using SubPub.Repository.LiteDb.Integration.Entities;

namespace SubPub.Mapping
{
    public class AutoMapperProfile : Profile
    {
        #region Public Constructors

        public AutoMapperProfile()
        {
            this.LiteDbIntegrationEntityToLiteDbIntegrationHistoryEntity();
        }

        #endregion Public Constructors

        #region Private Methods

        private void LiteDbIntegrationEntityToLiteDbIntegrationHistoryEntity()
        {
            CreateMap<LiteDbIntegrationEntity, LiteDbIntegrationHistoryEntity>()
                .ForMember(destination => destination.IntegrationId, options => options.MapFrom(source => source.Id));
        }

        #endregion Private Methods
    }
}

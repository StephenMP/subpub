﻿var SubPub = SubPub || {};
SubPub.Cards = SubPub.Cards || {};
SubPub.Snackbar = SubPub.Snackbar || {};

/* Cards */
SubPub.Cards.makeCardsEqualHeight = function () {
    var maxHeight = 0;

    $('.mdl-card').each(function () {
        var height = $(this).height();

        if (height > maxHeight) {
            maxHeight = height;
        }
    });

    $('.mdl-card').each(function () {
        $(this).css('height', maxHeight);
    });
};

/*Snackbar*/
SubPub.Snackbar.success = function (messageText, timeout, buttonText, eventHandler) {
    var snackbarJS = document.querySelector('#sp-snackbar');
    var snackbar = $('#sp-snackbar');
    var icon = $('#sp-snackbar__icon');

    icon.addClass('fa-smile');

    var data = {
        message: messageText,
        timeout: timeout
    };

    if (buttonText) {
        data.actionText = buttonText;
    }

    if (eventHandler) {
        data.actionHandler = eventHandler;
    }

    snackbarJS.MaterialSnackbar.showSnackbar(data);
};

SubPub.Snackbar.error = function (messageText, timeout, buttonText, eventHandler) {
    var snackbarJS = document.querySelector('#sp-snackbar');
    var snackbar = $('#sp-snackbar');
    var icon = $('#sp-snackbar__icon');

    icon.addClass('fa-frown');

    var data = {
        message: messageText,
        timeout: timeout
    };

    if (buttonText) {
        data.actionText = buttonText;
    }

    if (eventHandler) {
        data.actionHandler = eventHandler;
    }

    snackbarJS.MaterialSnackbar.showSnackbar(data);
};